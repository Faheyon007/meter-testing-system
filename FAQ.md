# Frequently Asked Questions (FAQ)

#### Case: Thingsboard becomes VERY SLOW after running a few weeks.

#### Case: Thingsboard Web Service is NOT STARTING after running a few weeks.

__Solution__: This problem is probably occurring due to the database used. _Thingsboard_ by default uses _HSQLDB_ which is great for development and testing purpose. However, it is not suitable for production and deployment. Using external database such as _PostgreSQL_ or _Cassandra_ should solve the problem.

See [External Database Installation](https://thingsboard.io/docs/user-guide/install/linux/#external-database-installation)

---

#### Case: No Telemetry Data is received.

#### Case: Stopped incoming of Telemetry data.

#### Case: Missing Telemetry data.

__Solution__: There are several reasons for this kind of behavior.

These includes (non-exhaustive):

- __Electrical issue__
  - Power cut off
  - Under voltage of Pi
- __MQTT Broker issue__
  - Disconnection
  - Rejection
  - Client ID collision
- __Master issue__
  - Hang
- __Network issue__
  - No internet access