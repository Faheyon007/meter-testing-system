##############################################################################
#                               Meter Testing System
##############################################################################
#
# Author        : Md. Farhabi Helal Ayon
# Date          : 25th November, 2018
#
# Objective     : Receive device data from MQTT & publish to ThingsBoard
#

##############################################################################
#                               IMPORTS
##############################################################################
from multiprocessing.dummy import Process
import datetime
import time
import logging
import logging.config
import json
import os
import os.path
import paho.mqtt.client as mqtt_client
import re
import sys
import copy
import requests





##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_FILENAME = "mqtt.conf"

MQTT_TOPIC_ELECTRICAL = 'E/Test/Elec'
MQTT_TOPIC_BILLING = 'E/Test/Billing'
MQTT_TOPIC_EVENT = 'E/Test/EventStatus'

MQTT_HOSTNAME = '119.148.42.42'
MQTT_PORT = 86
MQTT_USERNAME = ''
MQTT_PASSWORD = ''
MQTT_SUBSCRIBE_TOPICS = [ MQTT_TOPIC_ELECTRICAL, MQTT_TOPIC_BILLING, MQTT_TOPIC_EVENT ]

MQTT_CLIENT = mqtt_client.Client()



CONFIG_FILENAME = "core.conf"

TOTAL_METER_COUNT = 0
ACTIVE_METER_COUNT = 0
INACTIVE_METER_COUNT = 0

COOLDOWN_DURATION = 10 * 60 # seconds
UPDATE_INTERVAL = 1 * 60 # seconds

METER_LOG = dict()
METER_STATUS = dict()
RESET_LOG = dict()



CONFIG_FILENAME = "parser.conf"


METER_ID_START_INDEX = 1
METER_ID_END_INDEX = 7


CONFIG_FILENAME = "publisher.conf"

METER_COUNT_TRACKER_ACCESS_TOKEN = "wUgYEoptGVsJ2SDEasTU"
METER_STATE_TRACKER_ACCESS_TOKEN = "kQPDowsBfe5B8CZGhlO0"

ACCESS_TOKENS = [ METER_COUNT_TRACKER_ACCESS_TOKEN, METER_STATE_TRACKER_ACCESS_TOKEN ]

THINGSBOARD_HOSTNAME = "0.0.0.0"
THINGSBOARD_PORT = "8080"


PUBLISH_MAX_RETRY_COUNT = 3
PUBLISH_RETRY_INTERVAL = 3 # seconds





##############################################################################
#                               FUNCTIONS
##############################################################################
def mqtt_on_connect(client, userdata, flags, rc):
    logging.debug("Client connected")


def mqtt_on_disconnect(client, userdata, rc):
    logging.critical("Client disconnected.")
    logging.debug("Stopping loop...")
    MQTT_CLIENT.loop_stop()
    logging.debug("Loop stopped.")
    logging.debug("Reconnecting...")
    MQTT_CLIENT.connect(host=MQTT_HOSTNAME, port=MQTT_PORT)
    MQTT_CLIENT.loop_start()
    logging.debug("Reconnected successfully")


def mqtt_on_message(client, userdata, message):
    logging.info("New MQTT messeage received")

    logging.debug("Sending data to Core service for processing")
    
    try:
        core_on_message(message)
    except Exception as e:
        logging.exception(f"Exception from Core service during message processing.\n{e}")

    logging.debug("MQTT message processing completed")


def mqtt_init():
    MQTT_CLIENT.on_connect = mqtt_on_connect
    MQTT_CLIENT.on_disconnect = mqtt_on_disconnect
    MQTT_CLIENT.on_message = mqtt_on_message

    # logging.debug("Loading MQTT configurations...")
    # load_config()
    # logging.info("MQTT configurations loaded successfully")

    logging.debug(f"[ HOSTNAME ] : {MQTT_HOSTNAME}")
    logging.debug(f"[ PORT ] : {MQTT_PORT}")
    
    logging.debug("Connecting to MQTT broker...")
    MQTT_CLIENT.connect(host=MQTT_HOSTNAME, port=MQTT_PORT)
    logging.info("Connected to MQTT broker")

    logging.debug("Subscribing to topics...")
    for topic in MQTT_SUBSCRIBE_TOPICS:
        MQTT_CLIENT.subscribe(topic=topic)
        logging.debug(f"Subscribed to '{topic}'")

    logging.debug(f"Subscribe has completed")

    logging.debug("Starting loop...")
    # MQTT_CLIENT.loop_forever()
    MQTT_CLIENT.loop_start()
    logging.debug("Loop started successfully.")

    logging.debug("MQTT Initialization has completed.")




def parser_parse(data):
    try:
        meter_id_data = data[METER_ID_START_INDEX:METER_ID_END_INDEX]
        meter_id = ''.join([str(i) for i in meter_id_data])

        return meter_id
    except Exception as e:
        raise ValueError(f"data='{data}' is not in correct format.\n{e}")




def publisher_publish(data, token):

    # url = f'http://{THINGSBOARD_HOSTNAME}:{THINGSBOARD_PORT}/api/v1/{token}/telemetry'
    # logging.debug(f"[ URL ] : {url}")

    # try:
    #     data = json.dumps(data)
    # except Exception as e:
    #     # logging.error(sys.exc_info()[0])
    #     logging.exception(f"{e}")
    #     return False
    
    # logging.debug(f"[ DATA ] : {data}")
    # logging.debug("")
    
    # headers = {
    #     'Content-Type': 'application/json'
    # }
    
    # retry_counter = 1
    published = False
    # logging.debug("Publishing...")
    
    # try:
    #     r = requests.post(url=url, data=data, headers=headers)
    #     published = True if r.status_code == 200 else False
    # except Exception as e:
    #     published = False
    #     # logging.error(sys.exc_info()[0])
    #     logging.exception(f"{e}")

    # while not published:
    #     if retry_counter > PUBLISH_MAX_RETRY_COUNT:
    #         logging.debug("Publish attempt failed. Retry limit reached. Skipping publish")
    #         break
    #     logging.debug(f"Publish attempt failed. Retrying in {PUBLISH_RETRY_INTERVAL} seconds")
    #     time.sleep(PUBLISH_RETRY_INTERVAL)
    #     logging.debug(f"Retry {retry_counter}")
    #     try:
    #         r = requests.post(url=url, data=data, headers=headers)
    #         published = True if r.status_code == 200 else False
    #     except Exception as e:
    #         published = False
    #         # logging.error(sys.exc_info()[0])
    #         logging.exception(f"{e}")
    #     retry_counter = retry_counter + 1

    return published






def core_init():
    pass
    # logging.info("Core initialization has started")

    # global TOTAL_METER_COUNT
    # global ACTIVE_METER_COUNT
    # global INACTIVE_METER_COUNT
    # global METER_LOG
    # global COOLDOWN_DURATION
    # global UPDATE_INTERVAL
    # global METER_STATUS
    
    # TOTAL_METER_COUNT = 0
    # ACTIVE_METER_COUNT = 0
    # INACTIVE_METER_COUNT = 0

    # COOLDOWN_DURATION = 3 * 60 # seconds
    # UPDATE_INTERVAL = 1 * 60 # seconds
    
    # METER_STATUS = dict()
    # METER_LOG = dict()

    # logging.debug("Loading Core configurations...")
    # load_config()
    # logging.info("Core configurations loaded successfully")

    # logging.debug("Initializing Parser service")
    # parser.init()
    # logging.info("Parser service initialization complete")

    # logging.debug("Initializing Publisher service")
    # publisher.init()
    # logging.info("Publisher service initialization complete")

    # logging.debug("Starting Update service...")
    # update_process = Process(target=core_update)
    # update_process.start()
    # logging.info("Update service has started")
    
    # logging.debug("Core initialization has completed")

    # update_process.join()



def core_on_message(message):
    logging.debug("New message received")
    topic = message.topic
    payload = message.payload

    try:
        meter_id = parser_parse(payload) 
    except Exception as e:
        logging.exception(f"Exception during message parsing.\n{e}")
        return

    logging.debug("")
    logging.debug(f"[ TOPIC ] : {topic}")
    logging.debug(f"[ PAYLOAD ] : {[ str(i) for i in payload ]}")
    logging.debug(f"[ METER_ID ] : {meter_id}")
    logging.debug("")

    if meter_id not in METER_LOG:
        logging.debug(f"METER_ID='{meter_id}' not present in current METER_LOG")
        core_add_meter_id(meter_id)

    if topic in METER_LOG[meter_id]:
        logging.debug(f"TOPIC='{topic}' exists.")
        METER_LOG[meter_id][topic]['count'] = METER_LOG[meter_id][topic]['count'] + 1
        METER_LOG[meter_id][topic]['last_received'] = time.time()
    else:
        logging.debug(f"TOPIC='{topic}' does not exist.")
        METER_LOG[meter_id][topic] = dict()
        METER_LOG[meter_id][topic]['count'] = 1
        METER_LOG[meter_id][topic]['last_received'] = time.time()
        logging.debug(f"TOPIC='{topic}' created.")


    logging.debug("Message processing has completed")


def core_add_meter_id(meter_id):
    global METER_LOG
    METER_LOG[meter_id] = dict()
    logging.debug(f"Added METER_ID='{meter_id}' into METER_LOG")

    global TOTAL_METER_COUNT
    TOTAL_METER_COUNT = TOTAL_METER_COUNT + 1



def core_update():
    while True:
        logging.info("Update has started")


        core_print_status()

        global ACTIVE_METER_COUNT
        global INACTIVE_METER_COUNT

        global METER_LOG
        global METER_STATUS

        active_meter_count = 0
        inactive_meter_count = 0

        meter_log = copy.deepcopy(METER_LOG)

        for meter_id in METER_LOG:
            
            inactive = True
            
            for topic in METER_LOG[meter_id]:
                if int(time.time() - METER_LOG[meter_id][topic]['last_received']) < COOLDOWN_DURATION:
                    inactive = False
                    break
            
            if inactive:
                # del meter_log[meter_id]
                inactive_meter_count = inactive_meter_count + 1

            active_meter_count = TOTAL_METER_COUNT - inactive_meter_count
            
            METER_STATUS[meter_id] = "OFFLINE" if inactive else "ONLINE"

            METER_LOG = meter_log

            ACTIVE_METER_COUNT = active_meter_count
            INACTIVE_METER_COUNT = inactive_meter_count


        core_print_status()


        logging.debug("Publishing 'METER STATUS' data to ThingsBoard")
        data = METER_STATUS
        # prevents thingsboard api rejection
        if len(data) > 0:
            status = publisher_publish(data, METER_STATE_TRACKER_ACCESS_TOKEN)
            logging.debug("Publish complete" if status else "Publish failed")
        else:
            logging.debug("Nothing to publish")

        logging.debug("Publishing 'METER COUNT' data to ThingsBoard")
        data = core_get_meter_count_data()
        # prevents thingsboard api rejection
        if len(data) > 0:
            status = publisher_publish(data, METER_COUNT_TRACKER_ACCESS_TOKEN)
            logging.debug("Publish complete" if status else "Publish failed")
        else:
            logging.debug("Nothing to publish")

        logging.debug("Update has completed")

        time.sleep(UPDATE_INTERVAL)




def core_get_meter_count_data():
    return {
        "total_meter_count": TOTAL_METER_COUNT,
        "active_meter_count": ACTIVE_METER_COUNT,
        "inactive_meter_count": INACTIVE_METER_COUNT
    }



def core_print_status():

    logging.debug("")
    logging.debug("######################################################################################")
    logging.debug(f"\t\t\t\t\tSTATUS")
    logging.debug("######################################################################################")
    logging.debug("")

    logging.debug("")
    logging.debug("[ STATISTICS ]")
    logging.debug("")
    logging.debug("TOTAL METER COUNT : " + str(TOTAL_METER_COUNT))
    logging.debug("ACTIVE METER COUNT : " + str(ACTIVE_METER_COUNT))
    logging.debug("INACTIVE METER COUNT : " + str(INACTIVE_METER_COUNT))
    logging.debug("")

    logging.debug("")
    logging.debug("[ METER LOG ]")
    logging.debug("")

    for meter_id in METER_LOG:
        logging.debug(f"'{meter_id}':")
        for topic in METER_LOG[meter_id]:
            logging.debug(f"\t'{topic}':")
            logging.debug(f"\t\t'count': '{METER_LOG[meter_id][topic]['count']}'")
            logging.debug(f"\t\t'last_received': '{METER_LOG[meter_id][topic]['last_received']}'")


    logging.debug("")
    logging.debug("[ METER STATUS ]")
    logging.debug("")

    for meter_id in METER_STATUS:
        logging.debug(f"'{meter_id}': '{METER_STATUS[meter_id]}'")
            
    logging.debug("")
    logging.debug("")
    logging.debug("")






def init():

    log_directory_fullpath = os.path.join(os.getcwd(), "log")

    # create log directory if not exists
    if not os.path.exists(log_directory_fullpath):
        os.makedirs(log_directory_fullpath)

    config_directory_fullpath = os.path.join(os.getcwd(), "config")

    # create config directory if not exists
    if not os.path.exists(config_directory_fullpath):
        os.makedirs(config_directory_fullpath)

    logger_config_filename_fullpath = os.path.join(config_directory_fullpath, "logger.json")
    # create logger.json if not exists
    if not os.path.exists(logger_config_filename_fullpath):
        with open(logger_config_filename_fullpath, 'w') as file:
            file.write("")

    with open(logger_config_filename_fullpath) as logger_config_file:
        conf = json.load(logger_config_file)
    
    # handle empty filename
    if conf["handlers"]["file_handler"]["filename"] == "":
        conf["handlers"]["file_handler"]["filename"] = f'{os.path.join("log", datetime.datetime.now().strftime("%Y-%m-%d-%H:%M%p-%A"))}.log'

    logging.config.dictConfig(conf)
    
    # config.init()


def main():
    init()
    

    # start the mqtt service 
    logging.debug("Starting MQTT service...")
    # mqtt_process = Process(target=mqtt.init)
    mqtt_init()
    # mqtt_process.start()
    logging.info("MQTT service has started")

    
    # optional wait before starting mqtt service
    time.sleep(10)

    # start the core
    logging.debug("Starting Core service...")
    # core_init()
    # logging.debug("Core service has started")
    core_update()
  


if __name__ == "__main__":
    main()