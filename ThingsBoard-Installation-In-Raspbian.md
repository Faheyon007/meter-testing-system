# ThingsBoard Installation In Raspbian

## Prerequisite

- Java 1.8 or higher



## Installation

### Java
```bash
sudo apt install openjdk-8-jdk
```

### PostgreSQL
```bash
sudo apt update
sudo apt install postgresql postgresql-contrib
sudo service postgresql start
```

Once PostgreSQL is installed set the password for the the `postgres` user.

```bash
sudo -i -u postgres
psql
\password
# enter password
\q
```

Then, create the `thingsboard` database.

```bash
psql -U postgres -d postgres -h 127.0.0.1 -W

CREATE DATABASE thingsboard;
\q
```

### ThingsBoard
```bash
# Download the package
wget https://github.com/thingsboard/thingsboard/releases/download/v2.1/thingsboard-2.1.deb

# Install ThingsBoard as a service
sudo dpkg -i thingsboard-2.1.deb

# Update ThingsBoard memory usage and restrict it to 150MB in /etc/thingsboard/conf/thingsboard.conf
export JAVA_OPTS="$JAVA_OPTS -Dplatform=rpi -Xms256M -Xmx256M"

# --loadDemo option will load demo data: users, devices, assets, rules, widgets.
sudo /usr/share/thingsboard/bin/install/install.sh --loadDemo
```

## Configuration

### PostgreSQL

```bash
sudo nano /etc/thingsboard/conf/thingsboard.yml
```

Comment out `HSQLDB DAO` section

```
# HSQLDB DAO Configuration
#spring:
#  data:
#    jpa:
#      repositories:
#        enabled: "true"
#  jpa:
#    hibernate:
#      ddl-auto: "validate"
#    database-platform: "org.hibernate.dialect.HSQLDialect"
#  datasource:
#    driverClassName: "${SPRING_DRIVER_CLASS_NAME:org.hsqldb.jdbc.JDBCDriver}"
#    url: "${SPRING_DATASOURCE_URL:jdbc:hsqldb:file:${SQL_DATA_FOLDER:/tmp}/thingsboardDb;sql.enforce_size=false}"
#    username: "${SPRING_DATASOURCE_USERNAME:sa}"
#    password: "${SPRING_DATASOURCE_PASSWORD:}"
```

Uncomment `PostgresSQL DAO` section as well as update the database's `username` and `password`.

```
# PostgreSQL DAO Configuration
spring:
  data:
    jpa:
      repositories:
        enabled: "true"
  jpa:
    hibernate:
      ddl-auto: "validate"
    database-platform: "org.hibernate.dialect.PostgreSQLDialect"
  datasource:
    driverClassName: "${SPRING_DRIVER_CLASS_NAME:org.postgresql.Driver}"
    url: "${SPRING_DATASOURCE_URL:jdbc:postgresql://localhost:5432/thingsboard}"
    username: "${SPRING_DATASOURCE_USERNAME:postgres}"
    password: "${SPRING_DATASOURCE_PASSWORD:postgres}"
```

## Start ThingsBoard

```bash
sudo service thingsboard start
```

## Web UI

```
http://localhost:8080/
```

## Logs

```
/var/log/thingsboard
```


# Reference

- [Installing ThingsBoard on Raspberry Pi 3 Model B](https://thingsboard.io/docs/user-guide/install/rpi/)