##############################################################################
#                               IMPORTS
##############################################################################
import paho.mqtt.client as mqtt_client
import paho.mqtt.publish as mqtt_pub
import time
import random
from multiprocessing.dummy import  Process



##############################################################################
#                               GLOBALS
##############################################################################
MQTT_BROKER_HOSTNAME = "119.148.42.42"
MQTT_BROKER_PORT = 86

MQTT_TOPIC_ELECTRICAL = "Emulator/E/1/Elec"
MQTT_TOPIC_BILLING = "Emulator/E/1/Billing"
MQTT_TOPIC_EVENT = "Emulator/E/1/EventStatus"


METER_ID = [ random.randint(1, 255) for i in range(6)]


SAMPLE_ELECTRICAL_DATA = [ 39, 92, 207, 127, 35, 211, 127, 20, 0, 0, 8, 0, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 0, 0, 8, 0, 0, 0, 8, 0, 8, 0, 8, 8, 0, 80 ]
SAMPLE_BILLING_DATA = [ 39, 92, 207, 127, 35, 211, 127, 20, 00, 00, 8, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 8, 8, 8, 0, 8, 80 ]
SAMPLE_EVENT_DATE = [ 29, 92, 207, 127, 35, 211, 127, 20, 0, 0, 8, 0, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 98 ]



##############################################################################
#                               FUNCTIONS
##############################################################################
def emulate():
    print("[ LOG ] : Emulate has started")
    random.seed()
    
    client = mqtt_client.Client()

    print("\n[ MQTT BROKER HOSTNAME ] : " + MQTT_BROKER_HOSTNAME)
    print("[ MQTT BROKER PORT ] : " + str(MQTT_BROKER_PORT))

    print("\n[ LOG ] : Connecting MQTT client to Broker...")
    client.connect(host=MQTT_BROKER_HOSTNAME, port=MQTT_BROKER_PORT)
    print("[ LOG ] : MQTT client Connected\n")
    
    client.loop_start()

    
    
    
    while True:
        
        topic = MQTT_TOPIC_ELECTRICAL
        payload = SAMPLE_ELECTRICAL_DATA
        payload[1:7] = METER_ID
        
    
        # print("[ MQTT ] : Publishing...")
        print("\n[ TOPIC ] : " + topic)
        print("[ PAYLOAD ] : " + payload)
        client.publish(topic=topic, payload=payload)
        print("[ LOG ] : Published")

        time.sleep(10)


    client.loop_stop()

    print("[ LOG ] : Emulate has completed")





def main():
    print("[ LOG ] : Starting Device Emulator service...")    
    emulator_process = Process(target=emulate())
    emulator_process.start()
    print("[ LOG ] : Device Emulator service has started")





if __name__ == "__main__":
    main()