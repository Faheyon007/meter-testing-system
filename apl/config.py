##############################################################################
#                               IMPORTS
##############################################################################
import re
import sys
import os
import os.path
import json
import logging



##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_DIRECTORY = "config"
CONFIG_FILENAME = 'config.json'

CONFIG_PATTERN = r'(\w+)\s*\=\s*\"(.*)\"'
CONFIG_COMMENT_MARKER = ['#']
CONFIG_ARRAY_DELIMITER = ','



##############################################################################
#                               FUNCTIONS
##############################################################################


def init():
    logging.debug("Loading Config configurations...")
    load_config()
    logging.info("Config configurations loaded successfully")



def load_config():
    global CONFIG_PATTERN
    global CONFIG_COMMENT_MARKER
    global CONFIG_ARRAY_DELIMITER

    directory_fullpath = os.path.join(os.getcwd(), CONFIG_DIRECTORY)

    # if directory does not exist then create it
    if not os.path.exists(directory_fullpath):
        logging.warning(f"'{directory_fullpath}' does not exist")
        os.makedirs(directory_fullpath)
        logging.info(f"'{directory_fullpath}' created.")


    filename_fullpath = os.path.join(
        os.getcwd(), CONFIG_DIRECTORY, CONFIG_FILENAME)

    # if file does not exist then create it
    if not os.path.exists(filename_fullpath):
        logging.warning(f"'{filename_fullpath}' does not exist.")
        open(filename_fullpath, 'w')
        logging.info(f"'{filename_fullpath}' created.")


    with open(filename_fullpath) as file:
        conf = [ x.strip() for x in file.readlines() ]

    conf = json.loads("".join(conf))

    logging.debug("")
    logging.debug(f"[ CONFIG ] : {conf}")
    logging.debug("")

    for key in conf:
        if key == 'pattern':
            CONFIG_PATTERN = conf[key]

        elif key == 'comment_markers':
            CONFIG_COMMENT_MARKER = conf[key]

        elif key == 'array_delimiters':
            CONFIG_ARRAY_DELIMITER = conf[key]


    logging.debug("")
    logging.debug(f"[ CONFIG_PATTERN ] : {CONFIG_PATTERN}")
    logging.debug(f"[ CONFIG COMMENT MARKER ] : {CONFIG_COMMENT_MARKER}")
    logging.debug(f"[ CONFIG ARRAY DELIMITER ] : {CONFIG_ARRAY_DELIMITER}")
    logging.debug("")


def read_config(filename):
    logging.debug(f"Reading configurations from '{filename}'")

    directory_fullpath = os.path.join(os.getcwd(), CONFIG_DIRECTORY)

    # if directory does not exist then create it
    if not os.path.exists(directory_fullpath):
        logging.warning(f"'{directory_fullpath}' does not exist")
        os.makedirs(directory_fullpath)
        logging.info(f"'{directory_fullpath}' created.")


    filename_fullpath = os.path.join(
        os.getcwd(), CONFIG_DIRECTORY, filename)

    # if file does not exist then create it
    if not os.path.exists(filename_fullpath):
        logging.warning(f"'{filename_fullpath}' does not exist.")
        open(filename_fullpath, 'w')
        logging.info(f"'{filename_fullpath}' created.")

    config = {}
    with open(filename_fullpath) as file:
        for line in file:
            line = line.strip()
            if not line.startswith(tuple(CONFIG_COMMENT_MARKER)) or line is not '':
                match = re.match(CONFIG_PATTERN, line)
                if bool(match):
                    key = match.group(1)
                    value = match.group(2)

                    for delim in CONFIG_ARRAY_DELIMITER:
                        if value.__contains__(delim):
                            value = [x.strip() for x in value.split(
                                delim) if x is not '']

                    config[key] = value

    return config


def main():
    init()


if __name__ == "__main__":
    main()
