##############################################################################
#                               IMPORTS
##############################################################################
from multiprocessing.dummy import Process
import time
import copy
import logging

from apl import parser
from apl import publisher
from apl import config


##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_FILENAME = "core.conf"

TOTAL_METER_COUNT = 0
ACTIVE_METER_COUNT = 0
INACTIVE_METER_COUNT = 0

COOLDOWN_DURATION = 3 * 60  # seconds
UPDATE_INTERVAL = 1 * 60  # seconds

METER_LOG = dict()
METER_STATUS = dict()
RESET_LOG = dict()


##############################################################################
#                               FUNCTIONS
##############################################################################
def init():
    logging.info("Core initialization has started")

    global TOTAL_METER_COUNT
    global ACTIVE_METER_COUNT
    global INACTIVE_METER_COUNT
    global METER_LOG
    global COOLDOWN_DURATION
    global UPDATE_INTERVAL
    global METER_STATUS

    TOTAL_METER_COUNT = 0
    ACTIVE_METER_COUNT = 0
    INACTIVE_METER_COUNT = 0

    COOLDOWN_DURATION = 3 * 60  # seconds
    UPDATE_INTERVAL = 1 * 60  # seconds

    METER_STATUS = dict()
    METER_LOG = dict()

    logging.debug("Loading Core configurations...")
    load_config()
    logging.info("Core configurations loaded successfully")

    logging.debug("Initializing Parser service")
    parser.init()
    logging.info("Parser service initialization complete")

    logging.debug("Initializing Publisher service")
    publisher.init()
    logging.info("Publisher service initialization complete")

    logging.debug("Core initialization has completed")


def run():
    init()
    update()


def load_config():
    global UPDATE_INTERVAL
    global COOLDOWN_DURATION

    conf = config.read_config(CONFIG_FILENAME)

    logging.debug("")
    logging.debug(f"[ CONFIG ] : {conf}")
    logging.debug("")

    for key in conf:
        if key == 'update_interval':
            UPDATE_INTERVAL = int(conf[key])

        elif key == 'cooldown_duration':
            COOLDOWN_DURATION = int(conf[key])

    logging.debug("")
    logging.debug(f"[ UPDATE INTERVAL ] : {UPDATE_INTERVAL}")
    logging.debug(f"[ COOLDOWN DURATION ] : {COOLDOWN_DURATION}")
    logging.debug("")


def on_message(message):
    logging.debug("New message received")
    topic = message.topic
    payload = message.payload

    try:
        meter_id = parser.parse(payload)
    except Exception as e:
        logging.exception(f"Exception during message parsing.\n{e}")
        return

    logging.debug("")
    logging.debug(f"[ TOPIC ] : {topic}")
    logging.debug(f"[ PAYLOAD ] : {[ str(i) for i in payload ]}")
    logging.debug(f"[ METER_ID ] : {meter_id}")
    logging.debug("")

    if meter_id not in METER_LOG:
        logging.debug(
            f"METER_ID='{meter_id}' not present in current METER_LOG")
        add_meter_id(meter_id)

    if topic in METER_LOG[meter_id]:
        logging.debug(f"TOPIC='{topic}' exists.")
        METER_LOG[meter_id][topic]['count'] = METER_LOG[meter_id][topic]['count'] + 1
        METER_LOG[meter_id][topic]['last_received'] = time.time()
    else:
        logging.debug(f"TOPIC='{topic}' does not exist.")
        METER_LOG[meter_id][topic] = dict()
        METER_LOG[meter_id][topic]['count'] = 1
        METER_LOG[meter_id][topic]['last_received'] = time.time()
        logging.debug(f"TOPIC='{topic}' created.")

    logging.debug("Message processing has completed")


def add_meter_id(meter_id):
    global METER_LOG
    METER_LOG[meter_id] = dict()
    logging.debug(f"Added METER_ID='{meter_id}' into METER_LOG")

    global TOTAL_METER_COUNT
    TOTAL_METER_COUNT = TOTAL_METER_COUNT + 1


def update():
    while True:
        logging.info("Update has started")

        print_status()

        global ACTIVE_METER_COUNT
        global INACTIVE_METER_COUNT

        global METER_LOG
        global METER_STATUS

        active_meter_count = 0
        inactive_meter_count = 0

        meter_log = copy.deepcopy(METER_LOG)

        for meter_id in METER_LOG:

            inactive = True

            for topic in METER_LOG[meter_id]:
                if int(time.time() - METER_LOG[meter_id][topic]['last_received']) < COOLDOWN_DURATION:
                    inactive = False
                    break

            if inactive:
                # del meter_log[meter_id]
                inactive_meter_count = inactive_meter_count + 1

            active_meter_count = TOTAL_METER_COUNT - inactive_meter_count

            METER_STATUS[meter_id] = "OFFLINE" if inactive else "ONLINE"

            METER_LOG = meter_log

            ACTIVE_METER_COUNT = active_meter_count
            INACTIVE_METER_COUNT = inactive_meter_count

        print_status()

        logging.debug("Publishing 'METER STATUS' data to ThingsBoard")
        data = METER_STATUS
        # prevents thingsboard api rejection
        if len(data) > 0:
            status = publisher.publish(
                data, publisher.METER_STATE_TRACKER_ACCESS_TOKEN)
            logging.debug("Publish complete" if status else "Publish failed")
        else:
            logging.debug("Nothing to publish")

        logging.debug("Publishing 'METER COUNT' data to ThingsBoard")
        data = get_meter_count_data()
        # prevents thingsboard api rejection
        if len(data) > 0:
            status = publisher.publish(
                data, publisher.METER_COUNT_TRACKER_ACCESS_TOKEN)
            logging.debug("Publish complete" if status else "Publish failed")
        else:
            logging.debug("Nothing to publish")

        logging.debug("Update has completed")

        time.sleep(UPDATE_INTERVAL)


def get_meter_count_data():
    return {
        "total_meter_count": TOTAL_METER_COUNT,
        "active_meter_count": ACTIVE_METER_COUNT,
        "inactive_meter_count": INACTIVE_METER_COUNT
    }


def emulator():
    logging.info("Emulator has started")
    counter = 1

    while True:
        logging.debug("")
        logging.debug(
            "######################################################################################")
        logging.debug(f"\t\t\t\t\tROUND {counter}")
        logging.debug(
            "######################################################################################")
        logging.debug("")

        # Meter 1
        on_message({"topic": "test/meter/elec", "payload": "apl001"})
        time.sleep(1)
        on_message({"topic": "test/meter/billing", "payload": "apl001"})
        time.sleep(1)
        on_message({"topic": "test/meter/event", "payload": "apl001"})
        time.sleep(1)

        time.sleep(2)

        # Meter 2
        if counter <= 3 or counter >= 15:
            on_message({"topic": "test/meter/elec", "payload": "apl002"})
            time.sleep(1)
            on_message({"topic": "test/meter/billing", "payload": "apl002"})
            time.sleep(1)
            on_message({"topic": "test/meter/event", "payload": "apl002"})
            time.sleep(1)

            time.sleep(2)

        # Meter 3
        on_message({"topic": "test/meter/elec", "payload": "apl003"})
        time.sleep(1)
        on_message({"topic": "test/meter/billing", "payload": "apl003"})
        time.sleep(1)
        on_message({"topic": "test/meter/event", "payload": "apl003"})
        time.sleep(1)

        time.sleep(2)

        counter = counter + 1

    logging.info("Emulator has stopped")


def print_status():

    logging.debug("")
    logging.debug(
        "######################################################################################")
    logging.debug(f"\t\t\t\t\tSTATUS")
    logging.debug(
        "######################################################################################")
    logging.debug("")

    logging.debug("")
    logging.debug("[ STATISTICS ]")
    logging.debug("")
    logging.debug("TOTAL METER COUNT : " + str(TOTAL_METER_COUNT))
    logging.debug("ACTIVE METER COUNT : " + str(ACTIVE_METER_COUNT))
    logging.debug("INACTIVE METER COUNT : " + str(INACTIVE_METER_COUNT))
    logging.debug("")

    logging.debug("")
    logging.debug("[ METER LOG ]")
    logging.debug("")

    for meter_id in METER_LOG:
        logging.debug(f"'{meter_id}':")
        for topic in METER_LOG[meter_id]:
            logging.debug(f"\t'{topic}':")
            logging.debug(
                f"\t\t'count': '{METER_LOG[meter_id][topic]['count']}'")
            logging.debug(
                f"\t\t'last_received': '{METER_LOG[meter_id][topic]['last_received']}'")

    logging.debug("")
    logging.debug("[ METER STATUS ]")
    logging.debug("")

    for meter_id in METER_STATUS:
        logging.debug(f"'{meter_id}': '{METER_STATUS[meter_id]}'")

    logging.debug("")
    logging.debug("")
    logging.debug("")


def main():
    init()

    logging.debug("Starting Emulator service...")
    emulator_process = Process(target=emulator)
    emulator_process.start()
    logging.info("Emulator service has started")


if __name__ == "__main__":
    main()
