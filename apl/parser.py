##############################################################################
#                               IMPORTS
##############################################################################
import logging

from apl import config



##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_FILENAME = "parser.conf"


METER_ID_START_INDEX = 1
METER_ID_END_INDEX = 7



##############################################################################
#                               FUNCTIONS
##############################################################################
def init():
    logging.debug("Loading Parser configurations...")
    load_config()
    logging.info("Parser configurations loaded successfully")


def load_config():
    global METER_ID_START_INDEX
    global METER_ID_END_INDEX

    METER_ID_START_INDEX = 1
    METER_ID_END_INDEX = 7

    conf = config.read_config(CONFIG_FILENAME)
    logging.debug("")
    logging.debug(f"[ CONFIG ] : {conf}")
    logging.debug("")

    for key in conf:
        if key == 'start_byte':
            METER_ID_START_INDEX = int(conf[key])

        elif key == 'end_byte':
            METER_ID_END_INDEX = int(conf[key]) + 1

    logging.debug("")
    logging.debug(f"[ METER ID START INDEX ] : {METER_ID_START_INDEX}")
    logging.debug(f"[ METER ID END INDEX ] : {METER_ID_END_INDEX}")
    logging.debug("")


def parse(data):
    try:
        meter_id_data = data[METER_ID_START_INDEX:METER_ID_END_INDEX]
        meter_id = ''.join([str(i) for i in meter_id_data])

        return meter_id
    except Exception as e:
        raise ValueError(f"data='{data}' is not in correct format.\n{e}")



def main():
    init()
    data = [ 39, 92, 207, 127, 35, 211, 127, 20, 0, 00, 8, 0, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 0, 0, 8, 0, 0, 0, 8, 0, 8, 0, 8, 8, 0, 80 ]

    meter_id = parse(data)

    logging.info("[ DATA ] : " + str(data))
    logging.info("[ METER ID ] : " + meter_id)



if __name__ == "__main__":
    main()