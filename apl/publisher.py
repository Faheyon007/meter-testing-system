##############################################################################
#                               IMPORTS
##############################################################################
import requests
import time
import json
import sys
import logging

from apl import config



##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_FILENAME = "publisher.conf"

METER_COUNT_TRACKER_ACCESS_TOKEN = "wUgYEoptGVsJ2SDEasTU"
METER_STATE_TRACKER_ACCESS_TOKEN = "kQPDowsBfe5B8CZGhlO0"

ACCESS_TOKENS = [ METER_COUNT_TRACKER_ACCESS_TOKEN, METER_STATE_TRACKER_ACCESS_TOKEN ]

THINGSBOARD_HOSTNAME = "192.168.11.175"
THINGSBOARD_PORT = "8080"


PUBLISH_MAX_RETRY_COUNT = 3
PUBLISH_RETRY_INTERVAL = 3 # seconds



##############################################################################
#                               FUNCTIONS
##############################################################################
def init():
    logging.debug("Loading Publisher configurations...")
    load_config()
    logging.info("Publisher configurations loaded successfully")


def load_config():
    global THINGSBOARD_HOSTNAME
    global THINGSBOARD_PORT
    global PUBLISH_MAX_RETRY_COUNT
    global PUBLISH_RETRY_INTERVAL
    global ACCESS_TOKENS

    conf = config.read_config(CONFIG_FILENAME)
    logging.debug("")
    logging.debug("[ CONFIG ] : " + str(conf))
    logging.debug("")

    for key in conf:
        if key == 'hostname':
            THINGSBOARD_HOSTNAME = conf[key]

        elif key == 'port':
            THINGSBOARD_PORT = conf[key]

        elif key == 'retry_count':
            PUBLISH_MAX_RETRY_COUNT = int(conf[key])

        elif key == 'retry_interval':
            PUBLISH_RETRY_INTERVAL = int(conf[key])

        elif key == 'access_tokens':
            ACCESS_TOKENS = conf[key]


    logging.debug("")
    logging.debug(f"[ THINGSBOARD_HOSTNAME ] : {THINGSBOARD_HOSTNAME}")
    logging.debug(f"[ THINGSBOARD PORT ] : {THINGSBOARD_PORT}")
    logging.debug(f"[ PUBLISH MAX RETRY COUNT ] : {PUBLISH_MAX_RETRY_COUNT}")
    logging.debug(f"[ PUBLISH RETRY INTERVAL ] : {PUBLISH_RETRY_INTERVAL}")
    logging.debug(f"[ ACCESS TOKENS ] : {ACCESS_TOKENS}")
    logging.debug("")


def publish(data, token):

    url = f'http://{THINGSBOARD_HOSTNAME}:{THINGSBOARD_PORT}/api/v1/{token}/telemetry'
    logging.debug("[ URL ] : " + url)

    try:
        data = json.dumps(data)
    except Exception as e:
        # logging.error(sys.exc_info()[0])
        logging.exception(f"{e}")
        return False
    
    logging.debug("[ DATA ] : " + data)
    logging.debug("")
    
    headers = {
        'Content-Type': 'application/json'
    }
    
    retry_counter = 1
    published = False
    logging.debug("Publishing...")
    
    try:
        r = requests.post(url=url, data=data, headers=headers)
        published = True if r.status_code == 200 else False
    except Exception as e:
        published = False
        # logging.error(sys.exc_info()[0])
        logging.exception(f"{e}")

    while not published:
        if retry_counter > PUBLISH_MAX_RETRY_COUNT:
            logging.debug("Publish attempt failed. Retry limit reached. Skipping publish")
            break
        logging.debug(f"Publish attempt failed. Retrying in {PUBLISH_RETRY_INTERVAL} seconds")
        time.sleep(PUBLISH_RETRY_INTERVAL)
        logging.debug(f"Retry {retry_counter}")
        try:
            r = requests.post(url=url, data=data, headers=headers)
            published = True if r.status_code == 200 else False
        except Exception as e:
            published = False
            # logging.error(sys.exc_info()[0])
            logging.exception(f"{e}")
        retry_counter = retry_counter + 1

    return published


def main():
    TEST_DEVICE_ACCESS_TOKEN = "ngdjTIKUUFRcp8orqWvf"

    data = {
        "1234567890" : "ONLINE",
        "1234567891" : "ONLINE",
        "1234567892" : "OFFLINE",
        "1234567893" : "ONLINE"
    }

    publish(data, TEST_DEVICE_ACCESS_TOKEN)




if __name__ == "__main__":
    main()