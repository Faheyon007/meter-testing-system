##############################################################################
#                               IMPORTS
##############################################################################
import paho.mqtt.client as mqtt_client
import re
import logging
import sys
from time import sleep

from apl import core
from apl import config


##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_FILENAME = "mqtt.conf"

MQTT_TOPIC_ELECTRICAL = 'E/Test/Elec'
MQTT_TOPIC_BILLING = 'E/Test/Billing'
MQTT_TOPIC_EVENT = 'E/Test/EventStatus'

MQTT_HOSTNAME = '119.148.42.42'
MQTT_PORT = 86
MQTT_USERNAME = ''
MQTT_PASSWORD = ''
MQTT_SUBSCRIBE_TOPICS = [MQTT_TOPIC_ELECTRICAL,
                         MQTT_TOPIC_BILLING, MQTT_TOPIC_EVENT]
MQTT_PUBLISH_TOPICS = []

MQTT_CLIENT = mqtt_client.Client()

MQTT_CLIENT_CONNECTED = False


##############################################################################
#                               FUNCTIONS
##############################################################################


def on_connect(client, userdata, flags, rc):
    global MQTT_CLIENT_CONNECTED
    MQTT_CLIENT_CONNECTED = True
    logging.info(f"'{client}' connected to MQTT broker")

    logging.debug("Subscribing to topics...")
    for topic in MQTT_SUBSCRIBE_TOPICS:
        MQTT_CLIENT.subscribe(topic=topic)
        logging.debug(f"Subscribed to {topic}")
    logging.info("Subscription has completed.")


def on_disconnect(client, userdata, rc):
    global MQTT_CLIENT_CONNECTED
    MQTT_CLIENT_CONNECTED = False
    logging.critical(
        f"'{client}' disconnected from MQTT broker with rc={rc}")

    # logging.critical("Client disconnected.")
    # logging.debug("Stopping loop...")
    # MQTT_CLIENT.loop_stop()
    # logging.debug("Loop stopped.")
    # logging.debug("Reconnecting...")
    # MQTT_CLIENT.connect(host=MQTT_HOSTNAME, port=MQTT_PORT)
    # MQTT_CLIENT.loop_start()
    # logging.debug("Reconnected successfully")


def on_message(client, userdata, message):
    logging.info("New MQTT messeage received")

    logging.debug("Sending data to Core service for processing")

    try:
        core.on_message(message)
    except Exception as e:
        logging.exception(
            f"Exception from Core service during message processing.\n{e}")

    logging.debug("MQTT message processing completed")


def init():
    logging.info("MQTT initialization has started")

    MQTT_CLIENT.on_connect = on_connect
    MQTT_CLIENT.on_disconnect = on_disconnect
    MQTT_CLIENT.on_message = on_message

    logging.debug("Loading MQTT configurations...")
    load_config()
    logging.info("MQTT configurations loaded successfully")

    # logging.debug(f"[ HOSTNAME ] : {MQTT_HOSTNAME}")
    # logging.debug(f"[ PORT ] : {MQTT_PORT}")

    logging.debug("Connecting to MQTT broker...")
    MQTT_CLIENT.connect_async(host=MQTT_HOSTNAME, port=MQTT_PORT)
    # logging.info("Connected to MQTT broker")

    # logging.debug("Subscribing to topics...")
    # for topic in MQTT_SUBSCRIBE_TOPICS:
    #     MQTT_CLIENT.subscribe(topic=topic)
    #     logging.debug(f"Subscribed to {topic}")
    # logging.info("Subscription has completed.")

    logging.debug("Starting loop...")
    # MQTT_CLIENT.loop_forever()
    MQTT_CLIENT.loop_start()
    logging.debug("Loop started successfully.")

    logging.debug("Initialization has completed.")


def run():
    init()

    # wait time before start monitoring
    sleep(120)
    # start monitoring
    while True:
        logging.debug("MQTT Monitor has started")
        if not MQTT_CLIENT_CONNECTED:
            logging.warning(f"'{MQTT_CLIENT}' is not connected to the broker.")
            logging.debug("Stopping loop...")
            MQTT_CLIENT.loop_stop()
            logging.debug("Loop stopped.")
            init()
        else:
            logging.debug(f"'{MQTT_CLIENT}' is connected to the broker.")

        logging.debug("MQTT Monitor has completed")
        sleep(120)


def load_config():
    global MQTT_HOSTNAME
    global MQTT_PORT
    global MQTT_USERNAME
    global MQTT_PASSWORD
    global MQTT_SUBSCRIBE_TOPICS
    global MQTT_PUBLISH_TOPICS

    conf = config.read_config(CONFIG_FILENAME)
    logging.debug("")
    logging.debug(f"[ CONFIG ] : {conf}")
    logging.debug("")

    for key in conf:
        if key == 'hostname':
            MQTT_HOSTNAME = conf[key]

        elif key == 'port':
            MQTT_PORT = int(conf[key])

        elif key == 'username':
            MQTT_USERNAME = conf[key]

        elif key == 'password':
            MQTT_PASSWORD = conf[key]

        elif key == 'subscribe_topics':
            MQTT_SUBSCRIBE_TOPICS = conf[key]

        elif key == 'publish_topics':
            MQTT_PUBLISH_TOPICS = conf[key]

    logging.debug("")
    logging.debug(f"[ MQTT HOSTNAME ] : {MQTT_HOSTNAME}")
    logging.debug(f"[ MQTT PORT ] : {MQTT_PORT}")
    logging.debug(f"[ MQTT USERNAME ] : {MQTT_USERNAME}")
    logging.debug(f"[ MQTT PASSWORD ] : {MQTT_PASSWORD}")
    logging.debug(f"[ MQTT SUBSCRIBE TOPICS ] : {MQTT_SUBSCRIBE_TOPICS}")
    logging.debug(f"[ MQTT PUBLISH TOPICS ] : {MQTT_PUBLISH_TOPICS}")
    logging.debug("")


def main():
    init()


if __name__ == '__main__':
    main()
