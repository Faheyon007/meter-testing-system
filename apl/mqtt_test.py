##############################################################################
#                               IMPORTS
##############################################################################
import paho.mqtt.client as mqtt_client
import re
import logging
import sys
from multiprocessing.dummy import Process
from time import sleep



##############################################################################
#                               GLOBALS
##############################################################################
CONFIG_FILENAME = "mqtt.conf"

MQTT_TOPIC_ELECTRICAL = 'E/Test/Elec'
MQTT_TOPIC_BILLING = 'E/Test/Billing'
MQTT_TOPIC_EVENT = 'E/Test/EventStatus'

MQTT_HOSTNAME = '119.148.42.42'
MQTT_PORT = 86
MQTT_USERNAME = ''
MQTT_PASSWORD = ''
MQTT_SUBSCRIBE_TOPICS = [ MQTT_TOPIC_ELECTRICAL, MQTT_TOPIC_BILLING, MQTT_TOPIC_EVENT ]
MQTT_PUBLISH_TOPICS = []

MQTT_CLIENT = mqtt_client.Client()


logging.basicConfig(level=logging.DEBUG, format="%(asctime)s | %(levelname)-8s | %(message)s")

##############################################################################
#                               FUNCTIONS
##############################################################################

def on_connect(client, userdata, flags, rc):
    logging.debug("Client connected")

def on_message(client, userdata, message):
    logging.info("New MQTT messeage received")
    
    try:
        message = [ str(i) for i in message.payload ]
        logging.debug(f"[ MESSAGE RECEIVED ] : {message}")
    except Exception as e:
        logging.exception(f"{e}")

    logging.debug("MQTT message processing completed")

def on_disconnect(client, userdata, rc):
    logging.critical("Client disconnected.")
    logging.debug("Reconnecting...")
    MQTT_CLIENT.loop_stop()
    MQTT_CLIENT.connect(host=MQTT_HOSTNAME, port=MQTT_PORT)
    MQTT_CLIENT.loop_start()
    logging.debug("Reconnected successfully")

def init():
    MQTT_CLIENT.on_connect = on_connect
    MQTT_CLIENT.on_message = on_message
    MQTT_CLIENT.on_disconnect = on_disconnect

    logging.debug(f"[ HOSTNAME ] : {MQTT_HOSTNAME}")
    logging.debug(f"[ PORT ] : {MQTT_PORT}")
    
    logging.debug("Connecting to MQTT broker...")
    MQTT_CLIENT.connect(host=MQTT_HOSTNAME, port=MQTT_PORT)
    logging.info("Connected to MQTT broker")

    logging.debug("Subscribing to topics...")
    for topic in MQTT_SUBSCRIBE_TOPICS:
        MQTT_CLIENT.subscribe(topic=topic)
    logging.debug("Subscribed")

    # MQTT_CLIENT.loop_forever()
    MQTT_CLIENT.loop_start()
    logging.critical("Returning from Init")


def main():
    logging.debug("Starting MQTT process...")
    # proc = Process(target=init)
    # proc.start()
    init()
    logging.debug("MQTT process started")

    while True:
        # if not proc.is_alive():
        #     logging.critical("MQTT process is dead")
        #     try:
        #         logging.debug("Starting new MQTT process...")
        #         proc = Process(target=init)
        #         proc.start()
        #         logging.debug("MQTT process started")
        #     except Exception as e:
        #         print(f"{e}")
        # else:
        #     logging.debug(f"MQTT process is running.")

        logging.debug("Scheduled check completed.")
        sleep(60)


if __name__ == '__main__':
    main()