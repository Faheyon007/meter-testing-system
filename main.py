##############################################################################
#                               Meter Testing System
##############################################################################
#
# Author        : Md. Farhabi Helal Ayon
# Date          : 25th November, 2018
#
# Objective     : Receive device data from MQTT & publish to ThingsBoard
#

##############################################################################
#                               IMPORTS
##############################################################################
from multiprocessing.dummy import Process
import datetime
import time
import logging
import logging.config
import json
import os
import os.path

from apl import core
from apl import mqtt
from apl import config


##############################################################################
#                               FUNCTIONS
##############################################################################
def init():

    log_directory_fullpath = os.path.join(os.getcwd(), "log")

    # create log directory if not exists
    if not os.path.exists(log_directory_fullpath):
        os.makedirs(log_directory_fullpath)

    config_directory_fullpath = os.path.join(os.getcwd(), "config")

    # create config directory if not exists
    if not os.path.exists(config_directory_fullpath):
        os.makedirs(config_directory_fullpath)

    logger_config_filename_fullpath = os.path.join(
        config_directory_fullpath, "logger.json")
    # create logger.json if not exists
    if not os.path.exists(logger_config_filename_fullpath):
        with open(logger_config_filename_fullpath, 'w') as file:
            file.write("")

    with open(logger_config_filename_fullpath) as logger_config_file:
        conf = json.load(logger_config_file)

    # handle empty filename
    if conf["handlers"]["file_handler"]["filename"] == "":
        conf["handlers"]["file_handler"][
            "filename"] = f'{os.path.join("log", datetime.datetime.now().strftime("%Y-%m-%d-%H:%M%p-%A"))}.log'

    logging.config.dictConfig(conf)

    config.init()


def main():
    init()

    # start the mqtt service
    logging.debug("Starting MQTT service...")
    mqtt_process = Process(target=mqtt.run)
    mqtt_process.start()
    logging.info("MQTT service has started")

    # optional wait before starting mqtt service
    time.sleep(30)

    # start the core
    logging.debug("Starting Core service...")
    core_process = Process(target=core.run)
    core_process.start()
    logging.info("Core service has started")

    while True:
        if not mqtt_process.is_alive():
            logging.warning("MQTT service is dead.")
            # start the mqtt service
            logging.info("Starting MQTT service...")
            mqtt_process = Process(target=mqtt.run)
            mqtt_process.start()
            logging.info("MQTT service has started")

        if not core_process.is_alive():
            logging.warning("Core service is dead.")
            # start the core
            logging.info("Starting Core service...")
            core_process = Process(target=core.run)
            core_process.start()
            logging.info("Core service has started")

        time.sleep(30)


if __name__ == "__main__":
    main()
