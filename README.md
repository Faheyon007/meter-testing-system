# Meter Testing System

## Overview

- Receives real-time _Meter_ data via MQTT
- Maintains _Meter Configuration_ and _State_ internally
- Pushes _Meter Configuration_ and _State_ to _ThingsBoard_ at regular interval



## Prerequisites

- ThingsBoard
- Python 3.6 or higher
- Paho MQTT
- Requests


## Installation

### ThingsBoard

[ThingsBoard Installation in Raspbian](ThingsBoard-Installation-In-Raspbian.md)

### Python

Builds for Python 3.6 and higher are not available for Raspbian, yet. So, Python must be built from the source files.

Download the latest source tarball from official site and extract it.

```bash
wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tar.xz
tar xf Python-3.7.1.tar.xz
```

Install the required build dependencies.
```bash
sudo apt update
sudo apt install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev
```

Build Python

```bash
cd Python-3.7.1/
./configure --enable-optimizations
make -j4
sudo make install
```

Ensure Python is working
```bash
python -V
pip -V
```


### Dependencies

Install project dependencies via `pip`

```bash
sudo pip install paho-mqtt requests
```

## Run

From the project root directory, execute the following -

```bash
python main.py
```

## Auto Startup

To enable auto startup in Raspbian, execute the following -
```
sudo install-autostartup.sh
```

Make sure the script is executable. If not, then execute -
```bash
sudo chmod +x install-autostartup.sh
```

## Services

- Core
- MQTT
- Parser
- Publisher
- Config


## Configuration

_Directory_ : `/config`

Every service can be configured by config file.
The config file should have the same name as the service filename except ending with `.conf` extension

__For example__, _Core_ service is implemented in `core.py`. So, the config file must be named `core.conf`.

Configuration for `config` and `logging` services use `json`.

## Logs

_Directory_ : `/log`

Both console and file logging is supported. Logging behavior and content can be modified from the configuration.

Logging uses the `logging` module from the standard library. For more details, please see the manual.