#! /bin/bash

PWD="$(pwd)"
SCRIPT="$PWD/run.sh"
PYTHON="$PWD/env/bin/python"

rm -f "$SCRIPT"
echo "#! /bin/bash" >> "$SCRIPT"
echo "cd $PWD/" >> "$SCRIPT"
#echo "sleep 240s" >> "$SCRIPT"
echo "$PYTHON $PWD/main.py" >> "$SCRIPT"
chmod +x "$SCRIPT"

crontab -l | { cat; echo "@reboot '$SCRIPT'"; } | crontab -

if [ "$?" == "0" ] 
then 
    echo "Installed successfully."
else
    echo "Oops! Something went wrong. Installation failed."
fi
